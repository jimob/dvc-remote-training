import fire
import boto3
import time

ssm_client = boto3.client('ssm')

def execute_command(INSTANCE_ID, COMMAND):
    print("instance_id: ", INSTANCE_ID)
    print("command: ", COMMAND)

    command_id = _send_command(INSTANCE_ID, COMMAND)
    status = _wait_for_success(INSTANCE_ID, command_id)
    print(status)

def check_if_accessible(INSTANCE_ID):
    retries = 10
    retry_delay = 5
    retry_count = 0
    status = None
    while retry_count <= retries:
        retry_count += 1
        try:
            command_id = _send_command(INSTANCE_ID, "echo checking..")
            status = _get_command_status(INSTANCE_ID, command_id)
            if status == 'Success':
                print("Instance is UP & accessible")
                break
        except ssm_client.exceptions.InvalidInstanceId:
            print("instance is still not accessible retrying . . . ")
            time.sleep(retry_delay)
    if status != 'Success':
        raise Exception(f"instance {INSTANCE_ID} not accessible")


def _send_command(INSTANCE_ID, COMMAND):
    response = ssm_client.send_command(
                InstanceIds=[INSTANCE_ID],
                DocumentName="AWS-RunShellScript",
                Parameters={
                    'commands':[
                        COMMAND
                    ]
                    }
                )
    # we have to wait a second to request a status for the command
    time.sleep(1)
    return response['Command']['CommandId']


def _wait_for_success(INSTANCE_ID, command_id):
    status = _get_command_status(INSTANCE_ID, command_id)
    while status != 'Success':
        if status == 'Failed':
            raise Exception(f"command {command_id} not successful")
        print(f"waiting for command {command_id} to finish", flush=True)
        time.sleep(1)
        status = _get_command_status(INSTANCE_ID, command_id)
    return status
    

def _get_command_status(INSTANCE_ID, command_id):
    return ssm_client.get_command_invocation(
      CommandId=command_id,
      InstanceId=INSTANCE_ID,
    )['Status']

    

if __name__ == '__main__':
    fire.Fire()