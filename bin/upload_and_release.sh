RELEASE_NAME="$1"
BUCKET_NAME="$2"
PROJECT_ID="$3"
PRIVATE_TOKEN="$4"

### Upload DVC File to s3-bucket
aws s3 cp train.dvc s3://${BUCKET_NAME}/releases/${RELEASE_NAME}/

### Create Gitlab release Page with gitlab api
DESCRIPTION="link: https://${BUCKET_NAME}.s3-eu-west-1.amazonaws.com/releases/${RELEASE_NAME}/train.dvc"

curl --request POST\
     --header 'Content-Type: application/json'\
     --header "Private-Token: ${PRIVATE_TOKEN}"\
     --data-binary "{\"name\": \"${RELEASE_NAME}\", \"tag_name\": \"${RELEASE_NAME}\", \"description\": \"${DESCRIPTION}\"}"\
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases" 
