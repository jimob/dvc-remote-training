numpy==1.17.2
Pillow==6.1.0
Keras==2.2.5
Keras-Preprocessing==1.1.0
onnxmltools==1.5.0
keras2onnx==1.5.1
tensorflow==1.14.0