import os
import onnxmltools
import keras2onnx
import json

from pathlib import Path

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

from preprocessing import get_train_datagenerator

DIR_PATH = Path().absolute()
PATH_TO_TRAIN = Path.joinpath(DIR_PATH,'data/fruits/training')
PATH_TO_MODELS = Path.joinpath(DIR_PATH, 'models')
PATH_TO_CONFIG = Path.joinpath(DIR_PATH, 'config/train_config.json')

with open(PATH_TO_CONFIG) as f:
    train_config = json.load(f)

batch_size = train_config['batch_size']
target_size = tuple(train_config['target_size'])
epochs = train_config['epochs']
normalize_pixel_values = train_config['normalize_pixel_values']

input_shape = target_size + (3,)

train_generator = get_train_datagenerator(normalize_pixel_values)
train_generator = train_generator.flow_from_directory(
        PATH_TO_TRAIN,  
        target_size=target_size,  
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=True)

model = Sequential()
model.add(
    Conv2D(
        filters=8, 
        kernel_size=(3, 3), 
        padding='same', 
        input_shape=input_shape))
model.add(Activation('relu'))
model.add(
    Conv2D(
        filters=8, 
        kernel_size=(3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))

model.add(Flatten())
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(train_generator.num_classes, activation='softmax'))
    
model.compile(
    loss='categorical_crossentropy', 
    optimizer='adam', 
    metrics=['accuracy']
    )

model.fit_generator(
        generator=train_generator,
        steps_per_epoch= train_generator.samples // batch_size,
        epochs=epochs)

# convert to onnx model
onnx_model = keras2onnx.convert_keras(model, 'myModel.onnx')

# save onnx model
temp_model_file = 'model.onnx'
onnxmltools.utils.save_model(onnx_model, Path.joinpath(PATH_TO_MODELS, 'model.onnx'))

# save model config
model_config = {}
model_config['normalize_pixel_values']= normalize_pixel_values
model_config['target_size']=target_size
model_config['labels']=train_generator.class_indices

model_config = json.dumps(model_config)
with open(Path.joinpath(PATH_TO_MODELS, 'model_config.json'), 'w') as json_file:
    json_file.write(model_config)