import numpy as np

def get_class_prediction(labels, pred_probas):
    predicted_class_indices=np.argmax(pred_probas, axis=1)
    label_dic = dict((v,k) for k,v in labels.items())
    return [label_dic[k] for k in predicted_class_indices]